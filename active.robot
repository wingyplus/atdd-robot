*** Setting ***
Library           Selenium2Library
Suite setup       Open browser    about:blank    ${BROWSER}    remote_url=${HUBURL}
Test setup        Go to           ${URL}
Suite teardown    Close browser

*** Variable ***
${BROWSER}    chrome
${URL}        http://todomvc.com/examples/vue/#/all
${HUBURL}     ${NONE}

*** Test Cases ***
View Active
    Input Text             css=input.new-todo    สวัสดี 1
    Press Key              css=input.new-todo    \\13
    Input Text             css=input.new-todo    สวัสดี 2
    Press Key              css=input.new-todo    \\13
    Input Text             css=input.new-todo    สวัสดี 3
    Press Key              css=input.new-todo    \\13
    Click Element          css=ul.todo-list > li:nth-child(2) > div > input
    Click Element          css=ul.filters > li:nth-child(2) > a
    Page Should Contain    สวัสดี 1
    Page Should Contain    สวัสดี 3
