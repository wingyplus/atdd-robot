*** Setting ***
Library           Selenium2Library
Suite setup       Open browser    about:blank    ${BROWSER}    remote_url=${HUBURL}
Test setup        Go to           ${URL}
Suite teardown    Close browser

*** Variable ***
${BROWSER}    chrome
${URL}        http://todomvc.com/examples/vue/#/all
${HUBURL}     ${NONE}

*** Test Cases ***
View All
    Input Text             css=input.new-todo    สวัสดี 1
    Press Key              css=input.new-todo    \\13
    Input Text             css=input.new-todo    สวัสดี 2
    Press Key              css=input.new-todo    \\13
    Input Text             css=input.new-todo    สวัสดี 3
    Press Key              css=input.new-todo    \\13
    Click Element          css=.filters > li:nth-child(1) > a
    Page Should Contain    สวัสดี 1
    Page Should Contain    สวัสดี 2
    Page Should Contain    สวัสดี 3
