*** Settings ***
Library           Selenium2Library
Suite setup       Open browser    about:blank    ${BROWSER}    remote_url=${HUBURL}
Test setup        Go to           http://todomvc.com/examples/vue/
Suite teardown    Close browser

*** Variables ***
${HUBURL}    ${NONE}

*** Test Cases ***
Add new item
    New item    ทดสอบ
    ควรจะมีคำว่า "ทดสอบ" อยู่ในกล่องรายการ

Add 2 new items
    New item               นัดทานข้าว 12:00
    New item               ดูหนัง
    Page Should Contain    นัดทานข้าว 12:00
    Page Should Contain    ดูหนัง

*** Keywords ***
New item
    [Arguments]      ${NEW_ITEM_TEXT}
    ${ENTER_KEY}=    Set Variable          \\13
    Input Text       css=input.new-todo    ${NEW_ITEM_TEXT}
    Press Key        css=input.new-todo    ${ENTER_KEY}

ควรจะมีคำว่า "${NEW_ITEM_TEXT}" อยู่ในกล่องรายการ
    Page Should Contain    ${NEW_ITEM_TEXT}
